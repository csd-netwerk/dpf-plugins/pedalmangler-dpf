#include "DistrhoPluginPedalMangler.hpp"
#include "DistrhoUIPedalMangler.hpp"
#include <iostream>
#include <array>
#include <vector>
#include <math.h>
#include <string>

START_NAMESPACE_DISTRHO

namespace Art = DistrhoArtworkPedalMangler;

// -----------------------------------------------------------------------

DistrhoUIPedalMangler::DistrhoUIPedalMangler()
    : UI(Art::backgroundWidth, Art::backgroundHeight),
      fImgBackground(Art::backgroundData, Art::backgroundWidth, Art::backgroundHeight, GL_BGR)
{

    for (unsigned r = 0; r < 2; r++) {
        for (unsigned f = 0; f < 4; f++) {
            for (unsigned e = 0; e < 5; e++) {
                encoderValue[r][f][e] = 0.0;
            }
        }
    }


    encoderValue[0][0][1] = 0.0;
    encoderValue[0][0][2] = 0.0;
    encoderValue[0][0][3] = 0.308850;
    encoderValue[0][0][4] = 0.0;

    encoderValue[0][1][1] = 0.0;
    encoderValue[0][1][2] = 0.32034;
    encoderValue[0][1][3] = 0.49276;
    encoderValue[0][1][4] = 0.0;

    encoderValue[0][2][1] = 0.0;
    encoderValue[0][2][2] = 0.49925;
    encoderValue[0][2][3] = 1.0;
    encoderValue[0][2][4] = 1.0;

    encoderValue[0][3][1] = 0.0;
    encoderValue[0][3][2] = 0.0;
    encoderValue[0][3][3] = 0.0;
    encoderValue[0][3][4] = 1.0;

    for (unsigned fx = 0; fx < 4; fx++) {
        encoderValue[1][fx][1] = 0.1; // set Attack
        encoderValue[1][fx][2] = 1.0; // set Hold
        encoderValue[1][fx][3] = 0.1; // set Release
        encoderValue[1][fx][4] = 1.0; // set Mix
    }


    NanoVG::FontId font  = nvg.createFontFromMemory("notosans",
      font_notosans::notosans_ttf, font_notosans::notosans_ttf_size, false);
    nvg.fontFaceId(font);

    tabInit = true;
    startUp = true;

    int leftButtonXpos = 856;
    tabButton = new ImageSwitch(this,
            Image(Art::tabButtonOffData, Art::tabButtonOffWidth, Art::tabButtonOffHeight, GL_BGR),
            Image(Art::tabButtonOnData, Art::tabButtonOnWidth, Art::tabButtonOnHeight, GL_BGR));
    tabButton->setId(DistrhoPluginPedalMangler::paramTabButton);
    tabButton->setAbsolutePos(leftButtonXpos, 457);
    tabButton->setCallback(this);

    //learnButton = new ImageSwitch(this,
    //        Image(Art::tabButtonOffData, Art::tabButtonOffWidth, Art::tabButtonOffHeight, GL_BGR),
    //        Image(Art::tabButtonOnData, Art::tabButtonOnWidth, Art::tabButtonOnHeight, GL_BGR));
    //learnButton->setId(DistrhoPluginPedalMangler::paramLearn);
    //learnButton->setAbsolutePos(leftButtonXpos, 554);
    //learnButton->setCallback(this);
// EFFECT controls =====================================================

    int basePosEffectL1 = 116;
    int baseHeightEffect1 = 49;

    int basePosEffectL2 = 50;
    int baseHeightEffect2 = 116;

    int offsetA = 233;
    int offsetB = 0;
    Image knobImage(Art::knobData, Art::knobWidth, Art::knobHeight);

    gainKnob = new ImageKnob(this, knobImage, ImageKnob::Vertical);
    gainKnob->setId(DistrhoPluginPedalMangler::paramDistortionGain);
    gainKnob->setAbsolutePos(basePosEffectL1, baseHeightEffect1);
    gainKnob->setRange(0.5f, 10.0f);
    gainKnob->setDefault(5.5f);
    gainKnob->setValue(5.5f);
    gainKnob->setRotationAngle(305);
    gainKnob->setCallback(this);

    distKnob = new ImageKnob(this, knobImage, ImageKnob::Vertical);
    distKnob->setId(DistrhoPluginPedalMangler::paramDistortionThreshold);
    distKnob->setAbsolutePos(basePosEffectL2 + (offsetA * 0), baseHeightEffect2 + (offsetB * 1));
    distKnob->setRange(0.05f, 0.95f);
    distKnob->setDefault(0.67f);
    distKnob->setValue(0.67f);
    distKnob->setRotationAngle(305);
    distKnob->setCallback(this);

    phaserLFOKnob = new ImageKnob(this, knobImage, ImageKnob::Vertical);
    phaserLFOKnob->setId(DistrhoPluginPedalMangler::paramPhaserLfoFreq);
    phaserLFOKnob->setAbsolutePos(basePosEffectL1 + (offsetA * 1), baseHeightEffect1 + (offsetB * 1));
    phaserLFOKnob->setRange(0.00999999f, 5.0f);
    phaserLFOKnob->setDefault(2.0f);
    phaserLFOKnob->setValue(2.0f);
    phaserLFOKnob->setRotationAngle(305);
    phaserLFOKnob->setCallback(this);

    phaserFeedbackKnob = new ImageKnob(this, knobImage, ImageKnob::Vertical);
    phaserFeedbackKnob->setId(DistrhoPluginPedalMangler::paramPhaserFeedback);
    phaserFeedbackKnob->setAbsolutePos(basePosEffectL2 + (offsetA * 1), baseHeightEffect2 + (offsetB * 1));
    phaserFeedbackKnob->setRange(0.0f, 99.f);
    phaserFeedbackKnob->setDefault(55.0f);
    phaserFeedbackKnob->setValue(55.0f);
    phaserFeedbackKnob->setRotationAngle(305);
    phaserFeedbackKnob->setCallback(this);

    delayTimeKnob = new ImageKnob(this, knobImage, ImageKnob::Vertical);
    delayTimeKnob->setId(DistrhoPluginPedalMangler::paramDelayTime);
    delayTimeKnob->setAbsolutePos(basePosEffectL1 + (offsetA * 2), baseHeightEffect1 + (offsetB * 2));
    delayTimeKnob->setRange(50.0f, 2000.0f);
    delayTimeKnob->setDefault(488.0f);
    delayTimeKnob->setValue(488.0f);
    delayTimeKnob->setRotationAngle(305);
    delayTimeKnob->setCallback(this);

    delayFeedbackKnob = new ImageKnob(this, knobImage, ImageKnob::Vertical);
    delayFeedbackKnob->setId(DistrhoPluginPedalMangler::paramDelayFeedback);
    delayFeedbackKnob->setAbsolutePos(basePosEffectL2 + (offsetA * 2), baseHeightEffect2 + (offsetB * 2));
    delayFeedbackKnob->setRange(0.0f, 0.99f);
    delayFeedbackKnob->setDefault(0.6f);
    delayFeedbackKnob->setValue(0.6f);
    delayFeedbackKnob->setRotationAngle(305);
    delayFeedbackKnob->setCallback(this);

    reverbSizeKnob = new ImageKnob(this, knobImage, ImageKnob::Vertical);
    reverbSizeKnob->setId(DistrhoPluginPedalMangler::paramReverbRoomSize);
    reverbSizeKnob->setAbsolutePos(basePosEffectL1 + (offsetA * 3), baseHeightEffect1 + (offsetB * 3));
    reverbSizeKnob->setRange(0.0f, 1.0f);
    reverbSizeKnob->setDefault(0.75f);
    reverbSizeKnob->setValue(0.75f);
    reverbSizeKnob->setRotationAngle(305);
    reverbSizeKnob->setCallback(this);

    reverbMixKnob = new ImageKnob(this, knobImage, ImageKnob::Vertical);
    reverbMixKnob->setId(DistrhoPluginPedalMangler::paramReverbMix);
    reverbMixKnob->setAbsolutePos(basePosEffectL2 + (offsetA * 3), baseHeightEffect2 + (offsetB * 3));
    reverbMixKnob->setRange(0.0f, 1.0f);
    reverbMixKnob->setDefault(0.5f);
    reverbMixKnob->setValue(0.5f);
    reverbMixKnob->setRotationAngle(305);
    reverbMixKnob->setCallback(this);

    int rowHeight = 33;
    int posTop1 = 277;
    int posLeft = 48;
    int buttonWidth = 34;
    int endPos = 181;

//    Image sliderImage(Art::buttonOnData, Art::buttonOnWidth, Art::buttonOnHeight, GL_BGR);

    int knobRowHeight = 446;
    int knobRowBegin = 302;
    int knobRowOffset = 113;

    int encoderRowHeight = 424;
    int encoderRowBegin = 446;
    int encoderRowOffset = 96;

    encoder.push_back(new ImageKnob(this,
                     Image(Art::switchData, Art::switchWidth, Art::switchHeight, GL_BGR)));
    encoder[0]->setId(DistrhoPluginPedalMangler::paramEncoder1);
    encoder[0]->setAbsolutePos(knobRowBegin, knobRowHeight);
    encoder[0]->setAbsolutePos(291, 284);
    encoder[0]->setRange(0.0f, 3.0f);
    encoder[0]->setStep(1.0f);
    encoder[0]->setDefault(0.0f);
    encoder[0]->setImageLayerCount(4);
    encoder[0]->setCallback(this);

    encoder.push_back(new ImageKnob(this, knobImage, ImageKnob::Vertical));
    encoder[1]->setId(DistrhoPluginPedalMangler::paramEncoder2);
    encoder[1]->setAbsolutePos(encoderRowBegin + (encoderRowOffset * 0), encoderRowHeight);
    encoder[1]->setRange(0.0f, 1.0f);
    encoder[1]->setDefault(0.0f);
    encoder[1]->setValue(0.0f);
    encoder[1]->setRotationAngle(305);
    encoder[1]->setCallback(this);

    encoder.push_back(new ImageKnob(this, knobImage, ImageKnob::Vertical));
    encoder[2]->setId(DistrhoPluginPedalMangler::paramEncoder3);
    encoder[2]->setAbsolutePos(encoderRowBegin + (encoderRowOffset * 1), encoderRowHeight);
    encoder[2]->setRange(0.0f, 1.0f);
    encoder[2]->setDefault(0.0f);
    encoder[2]->setValue(0.0f);
    encoder[2]->setRotationAngle(305);
    encoder[2]->setCallback(this);

    encoder.push_back(new ImageKnob(this, knobImage, ImageKnob::Vertical));
    encoder[3]->setId(DistrhoPluginPedalMangler::paramEncoder4);
    encoder[3]->setAbsolutePos(encoderRowBegin + (encoderRowOffset * 2), encoderRowHeight);
    encoder[3]->setRange(0.0f, 1.0f);
    encoder[3]->setDefault(0.0f);
    encoder[3]->setValue(0.308f);
    encoder[3]->setRotationAngle(305);
    encoder[3]->setCallback(this);

    encoder.push_back(new ImageKnob(this, knobImage, ImageKnob::Vertical));
    encoder[4]->setId(DistrhoPluginPedalMangler::paramEncoder5);
    encoder[4]->setAbsolutePos(encoderRowBegin + (encoderRowOffset * 3), encoderRowHeight);
    encoder[4]->setRange(0.0f, 1.0f);
    encoder[4]->setDefault(0.0f);
    encoder[4]->setValue(0.0f);
    encoder[4]->setRotationAngle(305);
    encoder[4]->setCallback(this);

    //load all states
    for (int fx = 3; fx >= 0; fx--) {
        for (int e = 4; e >= 1; e--) {
            changeEncoderValue(encoder[e], encoderValue[1][fx][e]);
            encoder[e]->setValue(encoderValue[1][fx][e]);
        }
    }
    for (int fx = 3; fx >= 0; fx--) {
        for (int e = 4; e >= 1; e--) {
            changeEncoderValue(encoder[e], encoderValue[1][fx][e]);
            encoder[e]->setValue(encoderValue[0][fx][e]);
        }
    }
    changeEncoderValue(encoder[3], 0.308850);
    encoder[0]->setValue(0.308850);
}


DistrhoUIPedalMangler::~DistrhoUIPedalMangler()
{
}
// -----------------------------------------------------------------------
// DSP Callbacks

void DistrhoUIPedalMangler::parameterChanged(uint32_t index, float value)
{
    switch (index)
    {
    case DistrhoPluginPedalMangler::paramDistortionGain:
        gainKnob->setValue(value);
        break;
    case DistrhoPluginPedalMangler::paramDistortionThreshold:
        distKnob->setValue(value);
        break;
    case DistrhoPluginPedalMangler::paramPhaserLfoFreq:
        phaserLFOKnob->setValue(value);
        break;
    case DistrhoPluginPedalMangler::paramPhaserFeedback:
        phaserLFOKnob->setValue(value);
        break;
    case DistrhoPluginPedalMangler::paramDelayTime:
        delayTimeKnob->setValue(value);
        break;
    case DistrhoPluginPedalMangler::paramDelayFeedback:
        delayFeedbackKnob->setValue(value);
        break;
    case DistrhoPluginPedalMangler::paramReverbRoomSize:
        reverbSizeKnob->setValue(value);
        break;
    case DistrhoPluginPedalMangler::paramReverbMix:
        reverbMixKnob->setValue(value);
        break;
    case DistrhoPluginPedalMangler::paramEncoder1:
        encoder[0]->setValue(value);
        break;
    case DistrhoPluginPedalMangler::paramEncoder2:
        encoder[1]->setValue(value);
        break;
    case DistrhoPluginPedalMangler::paramEncoder3:
        encoder[2]->setValue(value);
        break;
    case DistrhoPluginPedalMangler::paramEncoder4:
        encoder[3]->setValue(value);
        break;
    case DistrhoPluginPedalMangler::paramEncoder5:
        encoder[4]->setValue(value);
        break;
    //case DistrhoPluginPedalMangler::paramLearn:
    //    learnButton->setDown(value);
    //    break;
    }
}

// -----------------------------------------------------------------------
// UI Callbacks

void DistrhoUIPedalMangler::uiIdle()
{
    static int blink_counter = 0;
    for (int i = 0; i < 4; i++)
    {
        if (buttonStatus[i] == 4) {
            if (blink_counter < 10) {
                blinkWhite = true;
            } else {
                blinkWhite = false;
            }
            repaint();
        }
    }
    blink_counter = (blink_counter + 1) % 20;
}

// -----------------------------------------------------------------------
// Widget Callbacks

void DistrhoUIPedalMangler::imageButtonClicked(ImageButton* button, int)
{
    //if (button != fButtonAbout)
    //    return;

}

void DistrhoUIPedalMangler::imageKnobDragStarted(ImageKnob* knob)
{
    editParameter(knob->getId(), true);
}

void DistrhoUIPedalMangler::imageKnobDragFinished(ImageKnob* knob)
{
    editParameter(knob->getId(), false);
}

void DistrhoUIPedalMangler::imageKnobValueChanged(ImageKnob* knob, float value)
{
    setParameterValue(knob->getId(), value);
}

void DistrhoUIPedalMangler::imageSliderDragStarted(ImageSlider* slider)
{
    editParameter(slider->getId(), true);
}

void DistrhoUIPedalMangler::imageSliderDragFinished(ImageSlider* slider)
{
    editParameter(slider->getId(), false);
}

void DistrhoUIPedalMangler::imageSliderValueChanged(ImageSlider* slider, float value)
{
    setParameterValue(slider->getId(), value);
}

void DistrhoUIPedalMangler::imageSwitchClicked(ImageSwitch* imageSwitch, bool down)
{
    const uint buttonId(imageSwitch->getId());

    editParameter(buttonId, true);
    setParameterValue(buttonId, down ? 1.0f : 0.0f);
    editParameter(buttonId, false);

    repaint();
}

void DistrhoUIPedalMangler::changeEncoderValue(ImageKnob* knob, float value)
{
    editParameter(knob->getId(), true);
    setParameterValue(knob->getId(), value);
    editParameter(knob->getId(), false);
}



void DistrhoUIPedalMangler::onDisplay()
{
    fImgBackground.draw();


    static int counter = 0;
    static int lightNumber = 0;

    if (counter == 10) {
        lightNumber++ % 4;
        counter = 0;
    }
    counter++;

    float r,g,b;

    r = 256.0f / 256;
    g = 256.0f / 256;
    b = 256.0f / 256;

    // print parameters
    nvg.beginFrame ( this );
    nvg.fontSize ( 22 );
    nvg.textAlign ( NanoVG::ALIGN_CENTER|NanoVG::ALIGN_MIDDLE );
    nvg.fillColor ( Color ( r, g, b ) );

    int rowOneMenuHeight = 342;
    int menuBeginPos = 438;
    float menuOffset = 95;

    if (!tabButton->isDown()) {
        nvg.textBox ( menuBeginPos + (menuOffset * 0), rowOneMenuHeight, 65.0f, "Method", nullptr );
        nvg.textBox ( menuBeginPos + (menuOffset * 1), rowOneMenuHeight, 65.0f, "Min", nullptr );
        nvg.textBox ( menuBeginPos + (menuOffset * 2), rowOneMenuHeight, 65.0f, "Max", nullptr );
        nvg.textBox ( menuBeginPos + (menuOffset * 3), rowOneMenuHeight, 65.0f, "Fade", nullptr );
    } else {
        nvg.textBox ( menuBeginPos + (menuOffset * 0), rowOneMenuHeight, 65.0f, "Attack", nullptr );
        nvg.textBox ( menuBeginPos + (menuOffset * 1), rowOneMenuHeight, 65.0f, "", nullptr );
        nvg.textBox ( menuBeginPos + (menuOffset * 2), rowOneMenuHeight, 65.0f, "Release", nullptr );
        nvg.textBox ( menuBeginPos + (menuOffset * 3), rowOneMenuHeight, 65.0f, "Mix ", nullptr );
    }

    r = 0.0f / 256;
    g = 0.0f / 256;
    b = 0.0f / 256;

    nvg.fillColor ( Color ( r, g, b ) );

    float y = rowOneMenuHeight + 30;

    char strBuf[100];

    if (!tabButton->isDown()) {

        if (tabInit || (int)encoder[0]->getValue() != prevFxLoopState) {
            for (unsigned e = 1; e < 5; e++) {
                if ((int)encoder[0]->getValue() != prevFxLoopState) {
                    //store previous settings
                    encoderValue[0][prevFxLoopState][e] = encoder[e]->getValue();
                } else {
                    //store previous settings from tab
                    if (!startUp) {
                        encoderValue[1][(int)encoder[0]->getValue()][e] = encoder[e]->getValue();
                    }
                }
                //load new state
                changeEncoderValue(encoder[e], encoderValue[0][(int)encoder[0]->getValue()][e]);
                encoder[e]->setValue(encoderValue[0][(int)encoder[0]->getValue()][e]);
            }
            startUp = false;
            tabInit = false;
            prevFxLoopState = (int)encoder[0]->getValue();
        }

        switch((int)floor(encoder[1]->getValue() + 0.5))
        {
            case 0:
                std::snprintf ( strBuf, 32, "Pitch");
                break;
            case 1:
                std::snprintf ( strBuf, 32, "Trans");
                break;
        }
        nvg.textBox ( menuBeginPos, y, 65.0f, strBuf, nullptr );

        float minMidiNote = (encoder[2]->getValue() * 87.f) + 21;

        std::snprintf ( strBuf, 32, noteSymbols[(int)minMidiNote % 12]);
        char octave[30];
        std::snprintf ( octave, 30, " %i", int(minMidiNote / 12.0f) - 1);
        strncat(strBuf, octave, sizeof(strBuf));
        nvg.textBox ( menuBeginPos + (menuOffset * 1), y, 65.0f, strBuf, nullptr );

        float maxMidiNote = (encoder[3]->getValue() * 87.f) + 21;

        std::snprintf ( strBuf, 32, noteSymbols[(int)maxMidiNote % 12]);
        std::snprintf ( octave, 30, " %i", int(maxMidiNote / 12.0f) - 1);
        strncat(strBuf, octave, sizeof(strBuf));
        nvg.textBox ( menuBeginPos + (menuOffset * 2), y, 65.0f, strBuf, nullptr );

        switch((int)floor(encoder[4]->getValue() + 0.5))
        {
            case 0:
                std::snprintf ( strBuf, 32, "Post");
                break;
            case 1:
                std::snprintf ( strBuf, 32, "Pre");
                break;
        }
        nvg.textBox ( menuBeginPos + (menuOffset * 3), y, 65.0f, strBuf, nullptr );

    } else {
        if (!tabInit || (int)encoder[0]->getValue() != prevFxLoopState) {
            for (unsigned e = 1; e < 5; e++) {
                if ((int)encoder[0]->getValue() != prevFxLoopState) {
                    //store previous settings
                    encoderValue[1][prevFxLoopState][e] = encoder[e]->getValue();
                } else {
                    //store previous settings from tab
                    encoderValue[0][(int)encoder[0]->getValue()][e] = encoder[e]->getValue();
                }
                //load new state
                changeEncoderValue(encoder[e], encoderValue[1][(int)encoder[0]->getValue()][e]);
                encoder[e]->setValue(encoderValue[1][(int)encoder[0]->getValue()][e]);
            }
            tabInit = true;
            prevFxLoopState = (int)encoder[0]->getValue();
        }

        for (unsigned e = 1; e < 5; e++) {
            if (e != 2) { //don't print hold param
                std::snprintf ( strBuf, 32, "%.2f", encoder[e]->getValue());
                nvg.textBox ( menuBeginPos + (menuOffset * (e - 1)), y, 65.0f, strBuf, nullptr );
            }
        }
    }

    //int greyBlockOffset = 33;
    //greyBlock.drawAt(80, 276);
    //greyBlock.drawAt(80 + (greyBlockOffset * 1), 276 + (greyBlockOffset * 1));
    //greyBlock.drawAt(80 + (greyBlockOffset * 2), 276 + (greyBlockOffset * 2));
    //greyBlock.drawAt(80 + (greyBlockOffset * 3), 276 + (greyBlockOffset * 3));

    //====================================================================
    r = 256.0f / 256;
    g = 256.0f / 256;
    b = 256.0f / 256;

    nvg.fillColor ( Color ( r, g, b ) );

    const int width  = getWidth();
    const int height = getHeight();

    Rectangle<int> rec;

    rec.setWidth(34);
    rec.setHeight(34);

    for (int i = 0; i < 4; i++)
    {
        int recX = 98;
        int recY = 327 + (64 * i);

        rec.setX(recX);

        // 1st
        rec.setY(recY);

        switch (buttonStatus[i])
        {
            case 0:
                glColor3f(159.0 / 255.0, 65.0 / 255.0, 66.0 / 255.0);
                setParameterValue(i, 0.f);
                break;
            case 1:
                glColor3f(63.0 / 255.0, 157.0 / 255.0, 64.0 / 255.0);
                setParameterValue(i, 1.f);
                break;
            case 2:
                glColor3f(150.0 / 255.0, 150.0 / 255.0, 57.0 / 255.0);
                setParameterValue(i, 2.f);
                break;
            case 3:
                glColor3f(45.0 / 255.0, 45.0 / 255.0, 141.0 / 255.0);
                setParameterValue(i, 3.f);
                break;
            case 4:
                if (blinkWhite) {
                    glColor3f(203.0 / 255.0, 204.0 / 255.0, 205.0 / 255.0);
                } else {
                    glColor3f(65.0 / 255.0, 65.0 / 255.0, 65.0 / 255.0);
                }
                break;
        }
        if (buttonStatus[i] <= 3) {
            std::snprintf ( strBuf, 32, "%i", buttonStatus[i] + 1);
        } else {
            std::snprintf ( strBuf, 32, " ");
        }
        nvg.textBox (recX - 16, recY +18, 65.0f, strBuf, nullptr );

        rec.draw();
    }

    nvg.endFrame();
}

bool DistrhoUIPedalMangler::onMouse(const MouseEvent& ev)
{
    if (ev.button != 1 || ! ev.press)
        return false;

    const int width  = getWidth();
    const int height = getHeight();

    Rectangle<int> r;

    r.setWidth(34);
    r.setHeight(34);

    for (int i = 0; i < 4; ++i)
    {
        r.setX(98);

        // 1st
        r.setY(327 + (64 * i));

        if (r.contains(ev.pos))
        {
            if (select) {
                prevStatus = buttonStatus[i];
                prevIndex = i;
                buttonStatus[i] = 4; //4 sets the button color to white
                select = false;
            } else {
                buttonStatus[prevIndex] = buttonStatus[i];
                buttonStatus[i] = prevStatus;
                select = true;
            }
            repaint();
            break;
        }
    }

    return true;
}
// -----------------------------------------------------------------------

UI* createUI()
{
    return new DistrhoUIPedalMangler();
}

// -----------------------------------------------------------------------

END_NAMESPACE_DISTRHO
