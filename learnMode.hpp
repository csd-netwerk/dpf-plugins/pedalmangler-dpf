#ifndef _H_LEARN_
#define _H_LEARN_

#define NUM_FX_LOOPS 4

class LearnMode {
public:
    LearnMode();
    ~LearnMode();
    void calibrate(int fxLoop, int input);
    void learn(int fxLoop, int input);
    int getMin(int fxLoop);
    int getMax(int fxLoop);
private:
    int min[NUM_FX_LOOPS];
    int max[NUM_FX_LOOPS];

};


#endif //_H_LEARN_
