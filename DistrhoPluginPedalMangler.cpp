#include "DistrhoPluginPedalMangler.hpp"
#include <iostream>


START_NAMESPACE_DISTRHO

// -----------------------------------------------------------------------

DistrhoPluginPedalMangler::DistrhoPluginPedalMangler()
    : Plugin(paramCount, 0, 0) // 0 programs, 0 states
{
    //matrix = new Matrix(getSampleRate()); //TODO make this variable

    distortionGain      = 5.5f;
    distortionThreshold = 0.67f;
    phaserLfoFreq       = 2.f;
    phaserFeedback      = 55.f;
    delayTime           = 488.f;
    delayFeedback       = 0.6f;
    reverbRoomSize      = 0.75f;
    reverbMix           = 0.5f;
    tabButton           = false;
    encoder4            = 0.44;
    learnMode           = false;

    //Tuna pitch tracker init
    tuna = new TunaMonoPitchTracker(getSampleRate());

    for (unsigned i = 0; i < 5; i++) {
        matrixRow[i] = i;
    }

    matrix.setSampleRate(getSampleRate());

    //init pitch ranges
    float distMinRange = 0.0;
    float distMaxRange = 0.308850;
    float distFadeMethod = 0.0;

    float phaserMinRange = 0.32034;
    float phaserMaxRange = 0.49276;
    float phaserFadeMethod = 0.0;

    float delayMinRange = 0.49925;
    float delayMaxRange = 1.0;
    float delayFadeMethod = 1.0;

    float reverbMinRange = 0.0;
    float reverbMaxRange = 0.0;
    float reverbFadeMethod = 1.0;

    matrix.setRange((distMinRange * 87.0) + 21.0, (distMaxRange * 87.0) + 21.0, 0);
    matrix.setFadeMethod((int)floor(distFadeMethod + 0.5), 0);
    matrix.setRange((phaserMinRange * 87.0) + 21.0, (phaserMaxRange * 87.0) + 21.0, 1);
    matrix.setFadeMethod((int)floor(phaserFadeMethod + 0.5), 1);
    matrix.setRange((delayMinRange * 87.0) + 21.0, (delayMaxRange * 87.0) + 21.0, 2);
    matrix.setFadeMethod((int)floor(delayFadeMethod + 0.5), 2);
    matrix.setRange((reverbMaxRange * 87.0) + 21.0, (reverbMaxRange * 87.0) + 21.0, 3);
    matrix.setFadeMethod((int)floor(reverbFadeMethod + 0.5), 3);

    // reset
    deactivate();
}

DistrhoPluginPedalMangler::~DistrhoPluginPedalMangler()
{
    delete tuna;
    tuna = nullptr;
}

// -----------------------------------------------------------------------
// Init

void DistrhoPluginPedalMangler::initParameter(uint32_t index, Parameter& parameter)
{
    switch (index)
    {
    case paramMatrixRow0:
        parameter.hints      = kParameterIsAutomable | kParameterIsInteger;
        parameter.name       = "MatrixRow0";
        parameter.symbol     = "MatrixRow0";
        parameter.ranges.def = 0;
        parameter.ranges.min = 0;
        parameter.ranges.max = 3;
        break;
    case paramMatrixRow1:
        parameter.hints      = kParameterIsAutomable | kParameterIsInteger;
        parameter.name       = "MatrixRow1";
        parameter.symbol     = "MatrixRow1";
        parameter.ranges.def = 0;
        parameter.ranges.min = 0;
        parameter.ranges.max = 3;
        break;
    case paramMatrixRow2:
        parameter.hints      = kParameterIsAutomable | kParameterIsInteger;
        parameter.name       = "MatrixRow2";
        parameter.symbol     = "MatrixRow2";
        parameter.ranges.def = 0;
        parameter.ranges.min = 0;
        parameter.ranges.max = 3;
        break;
    case paramMatrixRow3:
        parameter.hints      = kParameterIsAutomable | kParameterIsInteger;
        parameter.name       = "MatrixRow3";
        parameter.symbol     = "MatrixRow3";
        parameter.ranges.def = 0;
        parameter.ranges.min = 0;
        parameter.ranges.max = 3;
        break;
    case paramMatrixRow4:
        parameter.hints      = kParameterIsAutomable | kParameterIsInteger;
        parameter.name       = "MatrixRow4";
        parameter.symbol     = "MatrixRow4";
        parameter.ranges.def = 0;
        parameter.ranges.min = 0;
        parameter.ranges.max = 4;
        break;
    case paramChannelKnob:
        parameter.hints      = kParameterIsAutomable | kParameterIsInteger;
        parameter.name       = "ChannelKnob";
        parameter.symbol     = "ChannelKnob";
        parameter.ranges.def = 0;
        parameter.ranges.min = 0;
        parameter.ranges.max = 3;
        break;
    case paramDistortionGain:
        parameter.hints      = kParameterIsAutomable;
        parameter.name       = "distortionGain";
        parameter.symbol     = "distortionGain";
        parameter.unit       = "";
        parameter.ranges.def = 5.5f;
        parameter.ranges.min = 0.5f;
        parameter.ranges.max = 10.f;
        break;
    case paramDistortionThreshold:
        parameter.hints      = kParameterIsAutomable;
        parameter.name       = "distortionThreshold";
        parameter.symbol     = "distortionThreshold";
        parameter.unit       = "";
        parameter.ranges.def = 0.67f;
        parameter.ranges.min = 0.05f;
        parameter.ranges.max = 0.95f;
        break;
    case paramPhaserLfoFreq:
        parameter.hints      = kParameterIsAutomable;
        parameter.name       = "phaserLfoFreq";
        parameter.symbol     = "phaserLfoFreq";
        parameter.unit       = "";
        parameter.ranges.def = 2.f;
        parameter.ranges.min = 0.009999998f;
        parameter.ranges.max = 5.f;
        break;
    case paramPhaserFeedback:
        parameter.hints      = kParameterIsAutomable;
        parameter.name       = "phaserFeedback";
        parameter.symbol     = "phaserFeedback";
        parameter.unit       = "";
        parameter.ranges.def = 55.f;
        parameter.ranges.min = 0.f;
        parameter.ranges.max = 99.f;
        break;
    case paramDelayTime:
        parameter.hints      = kParameterIsAutomable;
        parameter.name       = "delayTime";
        parameter.symbol     = "delayTime";
        parameter.unit       = "";
        parameter.ranges.def = 488.f;
        parameter.ranges.min = 50.f;
        parameter.ranges.max = 2000.f;
        break;
    case paramDelayFeedback:
        parameter.hints      = kParameterIsAutomable;
        parameter.name       = "delayFeedback";
        parameter.symbol     = "delayFeedback";
        parameter.unit       = "";
        parameter.ranges.def = 0.6f;
        parameter.ranges.min = 0.f;
        parameter.ranges.max = 0.99f;
        break;
    case paramReverbRoomSize:
        parameter.hints      = kParameterIsAutomable;
        parameter.name       = "reverbRoomSize";
        parameter.symbol     = "reverbRoomSize";
        parameter.unit       = "";
        parameter.ranges.def = 0.75f;
        parameter.ranges.min = 0.f;
        parameter.ranges.max = 1.0f;
        break;
    case paramReverbMix:
        parameter.hints      = kParameterIsAutomable;
        parameter.name       = "reverbMix";
        parameter.symbol     = "reverbMix";
        parameter.unit       = "";
        parameter.ranges.def = 0.5f;
        parameter.ranges.min = 0.f;
        parameter.ranges.max = 1.f;
        break;
    case paramEncoder1:
        parameter.hints      = kParameterIsAutomable;
        parameter.name       = "Encoder1";
        parameter.symbol     = "Encoder1";
        parameter.ranges.def = 0.0f;
        parameter.ranges.min = 0.0f;
        parameter.ranges.max = 3.0f;
        break;
    case paramEncoder2:
        parameter.hints      = kParameterIsAutomable;
        parameter.name       = "Encoder2";
        parameter.symbol     = "Encoder2";
        parameter.ranges.def = 0.0f;
        parameter.ranges.min = 0.0f;
        parameter.ranges.max = 1.0f;
        break;
    case paramEncoder3:
        parameter.hints      = kParameterIsAutomable;
        parameter.name       = "Encoder3";
        parameter.symbol     = "Encoder3";
        parameter.ranges.def = 0.0f;
        parameter.ranges.min = 0.0f;
        parameter.ranges.max = 1.0f;
        break;
    case paramEncoder4:
        parameter.hints      = kParameterIsAutomable;
        parameter.name       = "Encoder4";
        parameter.symbol     = "Encoder4";
        parameter.ranges.def = 0.0f;
        parameter.ranges.min = 0.0f;
        parameter.ranges.max = 1.0f;
        break;
    case paramEncoder5:
        parameter.hints      = kParameterIsAutomable;
        parameter.name       = "Encoder5";
        parameter.symbol     = "Encoder5";
        parameter.ranges.def = 0.0f;
        parameter.ranges.min = 0.0f;
        parameter.ranges.max = 1.0f;
        break;
    case paramTabButton:
        parameter.hints      = kParameterIsAutomable|kParameterIsBoolean;
        parameter.name       = "tabButton";
        parameter.symbol     = "tabButton";
        parameter.ranges.def = 0.0f;
        parameter.ranges.min = 0.0f;
        parameter.ranges.max = 1.0f;
        break;
    case paramUseMIDI:
        parameter.hints      = kParameterIsAutomable|kParameterIsBoolean;
        parameter.name       = "UseMIDI";
        parameter.symbol     = "UseMIDI";
        parameter.ranges.def = 0.0f;
        parameter.ranges.min = 0.0f;
        parameter.ranges.max = 1.0f;
        break;
    case paramLearn:
        parameter.hints      = kParameterIsAutomable|kParameterIsBoolean;
        parameter.name       = "learnMode";
        parameter.symbol     = "learnMode";
        parameter.ranges.def = 0.0f;
        parameter.ranges.min = 0.0f;
        parameter.ranges.max = 1.0f;
        break;
    }
}

// -----------------------------------------------------------------------
// Internal data

float DistrhoPluginPedalMangler::getParameterValue(uint32_t index) const
{
    switch (index)
    {
    case paramMatrixRow0:
        return matrixRow[0];
    case paramMatrixRow1:
        return matrixRow[1];
    case paramMatrixRow2:
        return matrixRow[2];
    case paramMatrixRow3:
        return matrixRow[3];
    case paramMatrixRow4:
        return matrixRow[4];
    case paramChannelKnob:
        return channelKnob;
    case paramDistortionGain:
        return distortionGain;
    case paramDistortionThreshold:
        return distortionThreshold;
    case paramPhaserLfoFreq:
        return phaserLfoFreq;
    case paramPhaserFeedback:
        return phaserFeedback;
    case paramDelayTime:
        return delayTime;
    case paramDelayFeedback:
        return delayFeedback;
    case paramReverbRoomSize:
        return reverbRoomSize;
    case paramReverbMix:
        return reverbMix;
    case paramEncoder1:
        return encoder1;
    case paramEncoder2:
        return encoder2;
    case paramEncoder3:
        return encoder3;
    case paramEncoder4:
        return encoder4;
    case paramEncoder5:
        return encoder5;
    case paramTabButton:
        return tabButton;
    case paramUseMIDI:
        return useMIDI;
    case paramLearn:
        return learnMode;
    }

    return 0.0f;
}

void DistrhoPluginPedalMangler::setParameterValue(uint32_t index, float value)
{
    switch (index)
    {
    case paramMatrixRow0:
        matrixRow[0] = value;
        break;
    case paramMatrixRow1:
        matrixRow[1] = value;
        break;
    case paramMatrixRow2:
        matrixRow[2] = value;
        break;
    case paramMatrixRow3:
        matrixRow[3] = value;
        break;
    case paramMatrixRow4:
        matrixRow[4] = value;
        break;
    case paramChannelKnob:
        channelKnob = value;
        break;
    case paramDistortionGain:
        distortionGain = value;
        break;
    case paramDistortionThreshold:
        distortionThreshold = value;
        break;
    case paramPhaserLfoFreq:
        phaserLfoFreq = value;
        break;
    case paramPhaserFeedback:
        phaserFeedback = value;
        break;
    case paramDelayTime:
        delayTime = value;
        break;
    case paramDelayFeedback:
        delayFeedback = value;
        break;
    case paramReverbRoomSize:
        reverbRoomSize = value;
        break;
    case paramReverbMix:
        reverbMix = value;
        break;
    case paramEncoder1:
        encoder1 = value;
        break;
    case paramEncoder2:
        encoder2 = value;
        break;
    case paramEncoder3:
        encoder3 = value;
        break;
    case paramEncoder4:
        encoder4 = value;
        break;
    case paramEncoder5:
        encoder5 = value;
        break;
    case paramTabButton:
        tabButton = (bool)value;
        break;
    case paramUseMIDI:
        useMIDI = (bool)value;
        break;
    case paramLearn:
        learnMode = (bool)value;
        break;
    }
}

// -----------------------------------------------------------------------
// Process

void DistrhoPluginPedalMangler::activate()
{
}

void DistrhoPluginPedalMangler::deactivate()
{
}

void DistrhoPluginPedalMangler::midiNoteOn(float pitch, uint8_t velocity) {
    MidiEvent event;

    event.frame = 0;
    event.size = 3;
    event.data[0] = 0x90;
    event.data[1] = pitch;
    event.data[2] = velocity;
    writeMidiEvent(event);
}

void DistrhoPluginPedalMangler::midiNoteOff(float pitch) {
    MidiEvent event;

    event.frame = 0;
    event.size = 3;
    event.data[0] = 0x80;
    event.data[1] = pitch;
    event.data[2] = 0;
    writeMidiEvent(event);
}

void DistrhoPluginPedalMangler::run(const float** inputs, float** outputs, uint32_t frames,
                           const MidiEvent* events, uint32_t eventCount)
{

    matrix.setEffectParameters(0, distortionGain);
    matrix.setEffectParameters(1, distortionThreshold);
    matrix.setEffectParameters(2, phaserLfoFreq);
    matrix.setEffectParameters(3, phaserFeedback);
    matrix.setEffectParameters(4, delayTime);
    matrix.setEffectParameters(5, delayFeedback);
    matrix.setEffectParameters(6, reverbRoomSize);
    matrix.setEffectParameters(7, reverbMix);

    matrix.updateParameters();

    float inputBuffer[1024];
    float *input;

    float envelope[frames];
    float env_sum = 0.0f;
    bool isOnset = false;
    bool isOffset = false;

    input = inputBuffer;

    if (tabButton == 0) {
        //setAnalysisMethod(encoder2, encoder1);
        matrix.setRange((encoder3 * 87.0) + 21.0, (encoder4 * 87.0) + 21.0, encoder1);
        matrix.setFadeMethod((int)floor(encoder5 + 0.5), encoder1);
    } else {
        matrix.setASR(encoder2, encoder3, encoder4, encoder5, encoder1);
    }

    for (unsigned s = 0; s < frames; s++) {
        input[s] = inputs[0][s];

        envelope[s] = envfollow.process(input[s]);
        env_sum += envelope[s];
    }

    matrix.process(input, outputs, frames);

    env_sum = env_sum/frames;

    int detected_pitch = tuna->process((float*)input, frames);
    //prev_pitches[prev_pitch_head] = detected_pitch;

    float env_acc = envelope[frames - 1] - envelope[0]; //Envelope acceleration

    if (detected_pitch >= 38 && detected_pitch <= 90 && detected_pitch != playing_pitch) {
            // If the acceleration of the envelope is high, there is an onset
            isOnset = true;
    }
    if (env_sum < 0.05f) {
        // If the envelope (check the middle sample of the buffer) dropped to low, there is an offset
        isOffset = true;
    }

    if (useMIDI) {
        for (uint32_t i=0; i<eventCount; ++i) {

            if (events[i].data[0] == 0x90) {
                int midi_note = (int)events[i].data[1];
                matrix.setMidiInput(midi_note);
                //writeMidiEvent(events[i]);
            }
        }
    } else {
        if (isOnset) {
            //std::cout << "is Onset" << std::endl;
            if (playing_pitch) {
                //std::cout << "Midi Event: " << playing_pitch << " off" << std::endl;
                midiNoteOff(playing_pitch);
                //std::cout << "Midi Event: " << detected_pitch << " on" << std::endl;
                midiNoteOn(detected_pitch, 100);
            } else {
                //std::cout << "Midi Event: " << detected_pitch << " on" << std::endl;
                midiNoteOn(detected_pitch, 100);
            }
            playing_pitch = detected_pitch;
            matrix.setMidiInput((int)detected_pitch);
            //std::cout << "Midi Event: " << detected_pitch << " on" << std::endl;
        } else if (isOffset && playing_pitch) {
            //std::cout << "Midi Event: " << playing_pitch << " off" << std::endl;
            midiNoteOff(playing_pitch);
            playing_pitch = false;
        }
    }

    for (unsigned x = 0; x < 4; x++) {
        matrix.setRoutingOrder((int)matrixRow[x], x);
    }

    //struct MidiEvent event;

    ////rangeValues[(int)(encoder1 * 3)][0] = encoder3 * 127.0;
    ////rangeValues[(int)(encoder1 * 3)][1] = encoder4 * 127.0;


}

// -----------------------------------------------------------------------

Plugin* createPlugin()
{
    return new DistrhoPluginPedalMangler();
}

// -----------------------------------------------------------------------

END_NAMESPACE_DISTRHO
