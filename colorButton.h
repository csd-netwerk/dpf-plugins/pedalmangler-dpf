/*
 * Neko widget animation
 * Copyright (C) 2013-2015 Filipe Coelho <falktx@falktx.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * For a full copy of the GNU General Public License see the LICENSE file.
 */

#ifndef NEKO_WIDGET_HPP_INCLUDED
#define NEKO_WIDGET_HPP_INCLUDED

#include "DistrhoArtworkPedalMangler.hpp"

#include "Image.hpp"
#include "Widget.hpp"

#include <cstdlib> // rand
#include <iostream> // rand

using DGL_NAMESPACE::Image;

namespace Art = DistrhoArtworkPedalMangler;
//using namespace DistrhoArtworkPedalMangler;
// -----------------------------------------------------------------------

class ColorButton {
public:
    ColorButton(int defaultColor, int x, int y) :
          fCurImage(&fImages.redButton)
    {
        this->x = x;
        this->y = y;
        // load images
        {
            using namespace DistrhoArtworkPedalMangler;

            #define JOIN(a, b) a ## b
            #define LOAD_IMAGE(NAME) fImages.NAME.loadFromMemory(JOIN(NAME, Data), JOIN(NAME, Width), JOIN(NAME, Height));

            LOAD_IMAGE(redButton)
            LOAD_IMAGE(greenButton)
            LOAD_IMAGE(yellowButton)
            LOAD_IMAGE(blueButton)

            #undef JOIN
            #undef LOAD_IMAGE
        }
    }

    void setState(int state)
    {
        switch (state)
        {
            case 0:
                fCurImage = &fImages.redButton;
                break;
            case 1:
                fCurImage = &fImages.greenButton;
                break;
            case 2:
                fCurImage = &fImages.yellowButton;
                break;
            case 3:
                fCurImage = &fImages.blueButton;
                break;
        }
    }

    int getX()
    {
        return x;
    }

    int getY()
    {
        return y;
    }

    void draw(int state)
    {
        std::cout << "Drawing!!"<< std::endl;
        fCurImage->drawAt(x, y);
    }

private:
    int x, y;

    struct Images {
        Image redButton;
        Image greenButton;
        Image yellowButton;
        Image blueButton;
    } fImages;

    Image* fCurImage;
};

// -----------------------------------------------------------------------

#endif // NEKO_WIDGET_HPP_INCLUDED
