#ifndef DISTRHO_PLUGIN_NEKOBI_HPP_INCLUDED
#define DISTRHO_PLUGIN_NEKOBI_HPP_INCLUDED

#include "DistrhoPlugin.hpp"
#include "matrix.hpp"
#include "tuna_mono_pitch_tracker/tuna_mono_pitch_tracker.hpp"
#include "envFollower/envFollower.hpp"

#define NUM_FX_LOOPS 4

START_NAMESPACE_DISTRHO

// -----------------------------------------------------------------------

class DistrhoPluginPedalMangler : public Plugin
{
public:
    enum Parameters
    {
        paramMatrixRow0 = 0,
        paramMatrixRow1,
        paramMatrixRow2,
        paramMatrixRow3,
        paramMatrixRow4,
        paramChannelKnob,
        paramDistortionGain,
        paramDistortionThreshold,
        paramPhaserLfoFreq,
        paramPhaserFeedback,
        paramDelayTime,
        paramDelayFeedback,
        paramReverbRoomSize,
        paramReverbMix,
        paramEncoder1,
        paramEncoder2,
        paramEncoder3,
        paramEncoder4,
        paramEncoder5,
        paramTabButton,
        paramUseMIDI,
        paramLearn,
        paramCount
    };

    DistrhoPluginPedalMangler();
    ~DistrhoPluginPedalMangler() override;

protected:
    // -------------------------------------------------------------------
    // Information

    const char* getLabel() const noexcept override
    {
        return "PedalMangler";
    }

    const char* getDescription() const override
    {
        return "Pedal Switcher";
    }

    const char* getMaker() const noexcept override
    {
        return "Bram Giesen, Geert Roks, Haider Radja";
    }

    const char* getHomePage() const override
    {
        return "https://bramgiesen.com/pedalmangler";
    }

    const char* getLicense() const noexcept override
    {
        return "GPL v2+";
    }

    uint32_t getVersion() const noexcept override
    {
        return d_version(1, 1, 0);
    }

    int64_t getUniqueId() const noexcept override
    {
        return d_cconst('C', 'D', 'P', 'M');
    }

    // -------------------------------------------------------------------
    // Init

    void initParameter(uint32_t index, Parameter& parameter) override;

    // -------------------------------------------------------------------
    // Internal data

    float getParameterValue(uint32_t index) const override;
    void  setParameterValue(uint32_t index, float value) override;

    // -------------------------------------------------------------------
    // Process

    void activate() override;
    void deactivate() override;

    void midiNoteOn(float pitch, uint8_t velocity = 100);
    void midiNoteOff(float pitch);
    void run(const float** inputs, float**, uint32_t frames,
            const MidiEvent* events, uint32_t eventCount) override;

    // -------------------------------------------------------------------

private:
    float matrixRow[5];
    int matrixSettings[5] = {0, 1, 2, 3, 4};

    float distortionGain;
    float distortionThreshold;
    float phaserLfoFreq;
    float phaserFeedback;
    float delayTime;
    float delayFeedback;
    float reverbRoomSize;
    float reverbMix;
    float channelKnob;

    float encoder1;
    float encoder2;
    float encoder3;
    float encoder4;
    float encoder5;
    bool  tabButton;
    bool  learnMode;

    int bIndex = 0;
    double pSignal = 0.0;

    TunaMonoPitchTracker* tuna;
    EnvFollower envfollow;

    float playing_pitch = 0.0f;
    bool notePlaying = false;
    float prev_pitches[4];
    uint8_t prev_pitch_head = 0;
    bool useMIDI;


    Matrix matrix;

    DISTRHO_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR(DistrhoPluginPedalMangler)
};

// -----------------------------------------------------------------------

END_NAMESPACE_DISTRHO

#endif  // DISTRHO_PLUGIN_NEKOBI_HPP_INCLUDED
