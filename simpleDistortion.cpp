#include "simpleDistortion.hpp"

SimpleDistortion::SimpleDistortion() {

}

SimpleDistortion::~SimpleDistortion() {

}

void SimpleDistortion::process(const float* input, float* output, int frames) {
    for (int i = 0; i < frames; i++, input++, output++) {
        if(*input >= 0) {
            *output = std::min(*input * this->gain, this->thresh);
        } else {
            *output = std::max(*input * this->gain, -this->thresh);
        }
    }
}

void SimpleDistortion::setThreshold(float thresh) {
    this->thresh = thresh;
}
void SimpleDistortion::setGain(float gain) {
    this->gain = gain;
}

float SimpleDistortion::getThreshold() const{
    return this->thresh;
}
float SimpleDistortion::getGain() const{
    return this->gain;
}
