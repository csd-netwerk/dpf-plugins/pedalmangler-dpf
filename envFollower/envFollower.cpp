#include "envFollower.hpp"

EnvFollower::EnvFollower() {
}

EnvFollower::~EnvFollower() {
}

float EnvFollower::process(float in) {
    float out = std::abs(in);
    out = this->slider(out);
    return out;
}

float EnvFollower::slider(float in) {
    float out = this->prev_slideVal;

    if (in > out && this->rise_time != 0.0f) {
        out += (in - out) / ((this->rise_time / 1000) * this->samplerate);
    } else if ( in < out && this->fall_time != 0.0f) {
        out += (in - out) / ((this->fall_time / 1000) * this->samplerate);
    }

    this->prev_slideVal = out;

    return out;
}
