#ifndef DISTRHO_UI_NEKOBI_HPP_INCLUDED
#define DISTRHO_UI_NEKOBI_HPP_INCLUDED

#include "DistrhoUI.hpp"

#include "colorButton.h"
#include "NanoVG.hpp"
#include "NotoSans_Regular.ttf.hpp"
#include "ImageWidgets.hpp"
#include <vector>

#include "DistrhoArtworkPedalMangler.hpp"

using DGL_NAMESPACE::ImageAboutWindow;
using DGL_NAMESPACE::ImageButton;
using DGL_NAMESPACE::ImageKnob;
using DGL_NAMESPACE::ImageSlider;
using DGL_NAMESPACE::ImageSwitch;

#define NUM_TOGGLES 5

START_NAMESPACE_DISTRHO

// -----------------------------------------------------------------------

class DistrhoUIPedalMangler : public UI,
                        public ImageButton::Callback,
                        public ImageKnob::Callback,
                        public ImageSlider::Callback,
                        public ImageSwitch::Callback
{
public:
    DistrhoUIPedalMangler();
    ~DistrhoUIPedalMangler();

protected:
    // -------------------------------------------------------------------
    // DSP Callbacks

    void parameterChanged(uint32_t index, float value) override;

    // -------------------------------------------------------------------
    // UI Callbacks

    void uiIdle() override;

    // -------------------------------------------------------------------
    // Widget Callbacks
    bool onMouse(const MouseEvent& ev) override;
    void imageButtonClicked(ImageButton* button, int) override;
    void imageKnobDragStarted(ImageKnob* knob) override;
    void imageKnobDragFinished(ImageKnob* knob) override;
    void imageKnobValueChanged(ImageKnob* knob, float value) override;
    void imageSliderDragStarted(ImageSlider* slider) override;
    void imageSliderDragFinished(ImageSlider* slider) override;
    void imageSliderValueChanged(ImageSlider* slider, float value) override;
    void imageSwitchClicked(ImageSwitch* imageSwitch, bool down) override;
    void changeEncoderValue(ImageKnob* slider, float value);
    void onDisplay() override;

private:
    Image fImgBackground;
    Image greyBlock;
    Image blue;

    ScopedPointer<Image>       button1;
    ScopedPointer<ImageKnob>   channelKnob;
    ScopedPointer<ImageKnob>   gainKnob;
    ScopedPointer<ImageKnob>   distKnob;
    ScopedPointer<ImageKnob>   phaserLFOKnob;
    ScopedPointer<ImageKnob>   phaserFeedbackKnob;
    ScopedPointer<ImageKnob>   delayTimeKnob;
    ScopedPointer<ImageKnob>   delayFeedbackKnob;
    ScopedPointer<ImageKnob>   reverbSizeKnob;
    ScopedPointer<ImageKnob>   reverbMixKnob;

    ScopedPointer<ImageSwitch> tabButton1;
    ScopedPointer<ImageSwitch> toggle1;
    ScopedPointer<ImageSwitch> tabButton;
    ScopedPointer<ImageSwitch> learnButton;

    std::vector<ImageKnob*> encoder;

    bool blinkWhite = true;
    bool select = true;
    int prevStatus = 0;
    int prevIndex = 0;

    int buttonStatus[4] = {0,1,2,3};
    int previousChannel = 0;
    int prevFxLoopState = 0;
    bool tabInit;
    bool startUp;
    bool toggleStates[4] = {false, false, false, false};
    bool prevToggleStates[4] = {false, false, false, false};
    float encoderValue[2][4][5];
    const char* noteSymbols[12] = {"C", "C#", "D", "D#", "E", "F", "F#", "G", "G#", "A", "A#", "B"};

    ColorButton **colorButtons;

    bool fClicked[9];

    NanoVG nvg;

    DISTRHO_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR(DistrhoUIPedalMangler)
};

// -----------------------------------------------------------------------

END_NAMESPACE_DISTRHO

#endif // DISTRHO_UI_NEKOBI_HPP_INCLUDED
