#include "rangeCheck.hpp"

RangeChecker::RangeChecker()
{
}

RangeChecker::~RangeChecker()
{
}

void RangeChecker::setRange(float min, float max)
{
    range_min = min;
    range_max = max;
}

bool RangeChecker::withinRange(float input)
{
    if (input >= range_min && input <= range_max) {
        state = true;
    } else {
        state = false;
    }

    return state;
}
