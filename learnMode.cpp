#include "learnMode.hpp"

LearnMode::LearnMode()
{

}

LearnMode::~LearnMode()
{

}

void LearnMode::calibrate(int fxLoop, int input)
{
    min[fxLoop] = input;
    max[fxLoop] = input;

}

void LearnMode::learn(int fxLoop, int input)
{
    if (input > max[fxLoop]) {
        max[fxLoop] = input;
    }
    if (input < min[fxLoop]) {
        min[fxLoop] = input;
    }

}

int LearnMode::getMin(int fxLoop)
{
    return min[fxLoop];
}

int LearnMode::getMax(int fxLoop)
{
    return max[fxLoop];

}

