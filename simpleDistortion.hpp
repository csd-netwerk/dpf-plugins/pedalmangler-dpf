#ifndef _H_DISTORION_
#define _H_DISTORION_

#include <algorithm>

class SimpleDistortion {
public:
    SimpleDistortion();
    ~SimpleDistortion();

    void process(const float* input, float* output, int frames);

    void setThreshold(float thresh);
    void setGain(float gain);

    float getThreshold() const;
    float getGain() const;

private:
    float thresh = 0.8f;
    float gain = 0.5f;

};

#endif //_H_DISTORION_
