#ifndef _H_DELAY_
#define _H_DELAY_

class SimpleDelay {
public:
    SimpleDelay();
    ~SimpleDelay();

    void process(const float* input, float* output, int frames);

    void setDelayLength(int delay_ms);
    void setFeedback(float fdbck);

    int getDelayLength() const;
    float getFeedback() const;

private:
    void tick();

    float* delayBuffer;

    int delay_ms = 400;
    float fdbck = 0.5f;

    //float samplerate = 48000;

    int writehead = 0;
    int readhead = 96000 - (delay_ms*48000*0.001);
};

#endif //_H_DELAY_
