#!/usr/bin/make -f
# Makefile for DISTRHO Plugins #
# ---------------------------- #
# Created by falkTX
#

# --------------------------------------------------------------
# Project name, used for binaries

NAME = PedalMangler

# --------------------------------------------------------------
# Files to build

FILES_DSP = \
	DistrhoPluginPedalMangler.cpp \
	learnMode.cpp \
	ASR.cpp \
	matrix.cpp \
	rangeCheck.cpp \
	simpleDistortion.cpp \
	simpledelay.cpp \
	stonephaser.cpp \
	envFollower/envFollower.cpp \
	tuna_mono_pitch_tracker/tuna_mono_pitch_tracker.cpp \
	tuna_mono_pitch_tracker/fft.c \
	tuna_mono_pitch_tracker/spectr.c

FILES_UI  = \
	NotoSans_Regular.ttf.cpp \
	DistrhoArtworkPedalMangler.cpp \
	DistrhoUIPedalMangler.cpp \

# --------------------------------------------------------------
# Do some magic


include Makefile.mk

BUILD_CXX_FLAGS += -lfftw3 -lfftw3f -lm

# --------------------------------------------------------------
# Extra flags

LINK_FLAGS += -lpthread

# --------------------------------------------------------------

TARGETS += vst

all: $(TARGETS)

# --------------------------------------------------------------
