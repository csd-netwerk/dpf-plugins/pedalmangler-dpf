/* (Auto-generated binary data file). */

#ifndef BINARY_DISTRHOARTWORKPEDALMANGLER_HPP
#define BINARY_DISTRHOARTWORKPEDALMANGLER_HPP

namespace DistrhoArtworkPedalMangler
{
    extern const char* aboutData;
    const unsigned int aboutDataSize = 172710;
    const unsigned int aboutWidth    = 303;
    const unsigned int aboutHeight   = 190;

    extern const char* aboutButtonHoverData;
    const unsigned int aboutButtonHoverDataSize = 5888;
    const unsigned int aboutButtonHoverWidth    = 92;
    const unsigned int aboutButtonHoverHeight   = 16;

    extern const char* aboutButtonNormalData;
    const unsigned int aboutButtonNormalDataSize = 5888;
    const unsigned int aboutButtonNormalWidth    = 92;
    const unsigned int aboutButtonNormalHeight   = 16;

    extern const char* backgroundData;
    const unsigned int backgroundDataSize = 1683588;
    const unsigned int backgroundWidth    = 914;
    const unsigned int backgroundHeight   = 614;

    extern const char* sliderData;
    const unsigned int sliderDataSize = 6084;
    const unsigned int sliderWidth    = 39;
    const unsigned int sliderHeight   = 39;

    extern const char* buttonOnData;
    const unsigned int buttonOnDataSize = 3072;
    const unsigned int buttonOnWidth    = 32;
    const unsigned int buttonOnHeight   = 32;

    extern const char* buttonOnYellowData;
    const unsigned int buttonOnYellowDataSize = 1728;
    const unsigned int buttonOnYellowWidth    = 24;
    const unsigned int buttonOnYellowHeight   = 24;

    extern const char* buttonOffBlackData;
    const unsigned int buttonOffBlackDataSize = 1728;
    const unsigned int buttonOffBlackWidth    = 24;
    const unsigned int buttonOffBlackHeight   = 24;

    extern const char* buttonOffData;
    const unsigned int buttonOffDataSize = 1728;
    const unsigned int buttonOffWidth    = 24;
    const unsigned int buttonOffHeight   = 24;

    extern const char* knobData;
    const unsigned int knobDataSize = 10000;
    const unsigned int knobWidth    = 50;
    const unsigned int knobHeight   = 50;

    extern const char* knobSData;
    const unsigned int knobSDataSize = 6627;
    const unsigned int knobSWidth    = 47;
    const unsigned int knobSHeight   = 47;

    extern const char* toggleOff0Data;
    const unsigned int toggleOff0DataSize = 9570;
    const unsigned int toggleOff0Width    = 58;
    const unsigned int toggleOff0Height   = 55;

    extern const char* toggleOn0Data;
    const unsigned int toggleOn0DataSize = 9570;
    const unsigned int toggleOn0Width    = 58;
    const unsigned int toggleOn0Height   = 55;

    extern const char* toggleOffData;
    const unsigned int toggleOffDataSize = 9570;
    const unsigned int toggleOffWidth    = 58;
    const unsigned int toggleOffHeight   = 55;

    extern const char* toggleOnData;
    const unsigned int toggleOnDataSize = 9570;
    const unsigned int toggleOnWidth    = 58;
    const unsigned int toggleOnHeight   = 55;

    extern const char* toggleOff2Data;
    const unsigned int toggleOff2DataSize = 9570;
    const unsigned int toggleOff2Width    = 58;
    const unsigned int toggleOff2Height   = 55;

    extern const char* toggleOn2Data;
    const unsigned int toggleOn2DataSize = 9570;
    const unsigned int toggleOn2Width    = 58;
    const unsigned int toggleOn2Height   = 55;

    extern const char* toggleOff3Data;
    const unsigned int toggleOff3DataSize = 9570;
    const unsigned int toggleOff3Width    = 58;
    const unsigned int toggleOff3Height   = 55;

    extern const char* toggleOn3Data;
    const unsigned int toggleOn3DataSize = 9570;
    const unsigned int toggleOn3Width    = 58;
    const unsigned int toggleOn3Height   = 55;

    extern const char* toggleOff4Data;
    const unsigned int toggleOff4DataSize = 9570;
    const unsigned int toggleOff4Width    = 58;
    const unsigned int toggleOff4Height   = 55;

    extern const char* toggleOn4Data;
    const unsigned int toggleOn4DataSize = 9570;
    const unsigned int toggleOn4Width    = 58;
    const unsigned int toggleOn4Height   = 55;

    extern const char* channelSwitcherData;
    const unsigned int channelSwitcherDataSize = 92898;
    const unsigned int channelSwitcherWidth    = 78;
    const unsigned int channelSwitcherHeight   = 397;

    extern const char* switchData;
    const unsigned int switchDataSize = 93600;
    const unsigned int switchWidth    = 78;
    const unsigned int switchHeight   = 400;

    extern const char* tabButtonOnData;
    const unsigned int tabButtonOnDataSize = 3570;
    const unsigned int tabButtonOnWidth    = 35;
    const unsigned int tabButtonOnHeight   = 34;

    extern const char* tabButtonOffData;
    const unsigned int tabButtonOffDataSize = 3570;
    const unsigned int tabButtonOffWidth    = 35;
    const unsigned int tabButtonOffHeight   = 34;

    extern const char* greyBlockData;
    const unsigned int greyBlockDataSize = 3267;
    const unsigned int greyBlockWidth    = 33;
    const unsigned int greyBlockHeight   = 33;

    extern const char* colorButtonData;
    const unsigned int colorButtonDataSize = 5544;
    const unsigned int colorButtonWidth    = 22;
    const unsigned int colorButtonHeight   = 84;

    extern const char* redButtonData;
    const unsigned int redButtonDataSize = 1200;
    const unsigned int redButtonWidth    = 20;
    const unsigned int redButtonHeight   = 20;

    extern const char* greenButtonData;
    const unsigned int greenButtonDataSize = 1200;
    const unsigned int greenButtonWidth    = 20;
    const unsigned int greenButtonHeight   = 20;

    extern const char* yellowButtonData;
    const unsigned int yellowButtonDataSize = 1200;
    const unsigned int yellowButtonWidth    = 20;
    const unsigned int yellowButtonHeight   = 20;

    extern const char* blueButtonData;
    const unsigned int blueButtonDataSize = 1200;
    const unsigned int blueButtonWidth    = 20;
    const unsigned int blueButtonHeight   = 20;
}

#endif // BINARY_DISTRHOARTWORKPEDALMANGLER_HPP
