//
//  ADRS.h
//
//  Created by Nigel Redmon on 12/18/12.
//  EarLevel Engineering: earlevel.com
//  Copyright 2012 Nigel Redmon
//
//  For a complete explanation of the ADSR envelope generator and code,
//  read the series of articles by the author, starting here:
//  http://www.earlevel.com/main/2013/06/01/envelope-generators/
//
//  License:
//
//  This source code is provided as is, without warranty.
//  You may copy and distribute verbatim copies of this document.
//  You may modify and use this source code to create binary code for your own purposes, free or commercial.
//

#ifndef ADRS_h
#define ADRS_h

#include <iostream>


class ASR {
public:
	ASR(float samplerate);
	~ASR(void);
	float process(void);
    float getOutput(void);
    int getState(void);
	void gate(int on);
    void setMode(int mode);
    void setAttackRate(float rate);
    void setDecayRate(float rate);
    void setReleaseRate(float rate);
	void setSustainHold(float rate);
	void setSustainLevel(float level);
    void setTargetRatioA(float targetRatio);
    void setTargetRatioDR(float targetRatio);
    void reset(void);

    enum envState {
        env_idle = 0,
        env_attack,
        env_sustain,
        env_release
    };

protected:
	int state;
	int mode;
    float samplerate;
	float output;
	float attackRate;
	float decayRate;
	float releaseRate;
	float attackCoef;
	float decayCoef;
	float releaseCoef;
    float sustainHoldRate;
    float sustainHold = 0.0;
	float sustainLevel;
    float targetRatioA;
    float targetRatioDR;
    float attackBase;
    float decayBase;
    float releaseBase;

    float calcCoef(float rate, float targetRatio);
};

inline float ASR::process() {
	switch (state) {
        case env_idle:
            sustainHold = 0.0;
            break;
        case env_attack:
            sustainHold = 0.0;
            output = attackBase + output * attackCoef;
            if (output >= sustainLevel) {
                output = sustainLevel;
                state = env_sustain;
            }
            break;
        case env_sustain:
            sustainHold += sustainHoldRate;
            if (sustainHold >= 1.0 && mode >= 2) {
                sustainHold = 0.0;
                state = env_release;
            } else {
                output = sustainLevel;
            }
            break;
        case env_release:
            output = releaseBase + output * releaseCoef;
            sustainHold = 0.0;
            if (output <= 0.0) {
                output = 0.0;
                state = env_idle;
            }
	}
	return output;
}

inline void ASR::gate(int gate) {
	if (gate == 1) {
        if (state == env_release || state == env_idle) {
            state = env_attack;
        }
    }
	else if (gate == 0) {
        state = env_release;
    }
}

inline int ASR::getState() {
    return state;
}

inline void ASR::reset() {
    state = env_idle;
    output = 0.0;
}

inline float ASR::getOutput() {
	return output;
}

#endif
