#include "matrix.hpp"
#include <iostream>

Matrix::Matrix()
{
    reverb.setParameter(MVerb<float>::DAMPINGFREQ, 0.5f);
    reverb.setParameter(MVerb<float>::DENSITY, 0.5f);
    reverb.setParameter(MVerb<float>::BANDWIDTHFREQ, 0.5f);
    reverb.setParameter(MVerb<float>::DECAY, 0.5f);
    reverb.setParameter(MVerb<float>::PREDELAY, 0.5f);
    reverb.setParameter(MVerb<float>::SIZE, 0.75f);
    reverb.setParameter(MVerb<float>::GAIN, 1.0f);
    reverb.setParameter(MVerb<float>::MIX, 0.5f);
    reverb.setParameter(MVerb<float>::EARLYMIX, 0.5f);

    prevRoomSize = 0.95;
    prevDecay = 0.5;

    range = new RangeChecker*[MATRIX_SIZE-1];

    sends     = new float*[48000];
    returns   = new float*[48000];

    sends[0] = send1;
    sends[1] = send2;
    sends[2] = send3;
    sends[3] = send4;
    sends[4] = send5;
    returns[0] = return1;
    returns[1] = return2;
    returns[2] = return3;
    returns[3] = return4;
    returns[4] = return5;

    envelopes = new ASR*[4];

    for (unsigned e = 0; e < MATRIX_SIZE-1; e++) {

        matrixSettings[e] = e;
        routingOrder[e] = e;
        range[e] = new RangeChecker();
        //range[e]->setRange(0, 0);

        for (unsigned s = 0; s < 1024; s++) {
            dryBuffer[e][s] = 0.0;
            wetBuffer[e][s] = 0.0;
            sends[e][s] = 0.0;
            returns[e][s] = 0.0;
        }
    }
}

Matrix::Matrix(float sampleRate)
{
    phaser.init(sampleRate);
    reverb.setSampleRate(sampleRate);

    reverb.setParameter(MVerb<float>::DAMPINGFREQ, 0.5f);
    reverb.setParameter(MVerb<float>::DENSITY, 0.5f);
    reverb.setParameter(MVerb<float>::BANDWIDTHFREQ, 0.5f);
    reverb.setParameter(MVerb<float>::DECAY, 0.5f);
    reverb.setParameter(MVerb<float>::PREDELAY, 0.5f);
    reverb.setParameter(MVerb<float>::SIZE, 0.75f);
    reverb.setParameter(MVerb<float>::GAIN, 1.0f);
    reverb.setParameter(MVerb<float>::MIX, 0.5f);
    reverb.setParameter(MVerb<float>::EARLYMIX, 0.5f);

    range = new RangeChecker*[MATRIX_SIZE-1];

    sends     = new float*[48000];
    returns   = new float*[48000];

    sends[0] = send1;
    sends[1] = send2;
    sends[2] = send3;
    sends[3] = send4;
    sends[4] = send5;
    returns[0] = return1;
    returns[1] = return2;
    returns[2] = return3;
    returns[3] = return4;
    returns[4] = return5;

    envelopes = new ASR*[4];

    for (unsigned e = 0; e < MATRIX_SIZE-1; e++) {

        matrixSettings[e] = e;
        envelopes[e] = new ASR(sampleRate);
        range[e] = new RangeChecker();
        //range[e]->setRange(0, 0);

        for (unsigned s = 0; s < 1024; s++) {
            dryBuffer[e][s] = 0.0;
            wetBuffer[e][s] = 0.0;
            sends[e][s] = 0.0;
            returns[e][s] = 0.0;
        }
    }
}


Matrix::~Matrix()
{
    delete[] envelopes;
    envelopes = nullptr;
    delete[] sends;
    sends = nullptr;
    delete[] returns;
    returns = nullptr;
    delete[] range;
    range = nullptr;
}

void Matrix::setSampleRate(float sampleRate)
{
    phaser.init(sampleRate);
    reverb.setSampleRate(sampleRate);

    this->sampleRate = sampleRate;

    envelopes = new ASR*[4];

    for (unsigned e = 0; e < MATRIX_SIZE-1; e++) {

        envelopes[e] = new ASR(sampleRate);
    }
}

void Matrix::setASR(float attack, float hold, float release, float mix, int fxLoop)
{
    envelopes[fxLoop]->setAttackRate(attack * sampleRate);
    envelopes[fxLoop]->setSustainHold(hold * sampleRate);
    envelopes[fxLoop]->setReleaseRate(release * sampleRate);
    envelopes[fxLoop]->setSustainLevel(mix);
}

void Matrix::setFadeMethod(int value, int index)
{
    CVmodes[index] = value;
}

void Matrix::setRoutingOrder(int value, int index)
{
    routingOrder[index] = value;
}

void Matrix::setRange(int minimum, int maximum, int index)
{
    range[index]->setRange(minimum, maximum);
}

void Matrix::setMatrix(int value, int index)
{
    //matrixSettings[index] = value;
}

void Matrix::setMidiInput(int midiPitch)
{
    this->midiPitch = midiPitch;
}

void Matrix::setEffectParameters(int index, float value)
{
    switch((EffectParameters)index)
    {
        case distortionGain:
            distGain = value;
            break;
        case distortionThreshold:
            distThreshold = value;
            break;
        case phaserLfoFreq:
            pLfoFreq = value;
            break;
        case phaserFeedback:
            pFeedback = value;
            break;
        case delayTime:
            dTime = value;
            break;
        case delayFeedback:
            dFeedback = value;
            break;
        case reverbRoomSize:
            verbRoomSize = value;
            break;
        case reverbDecay:
            verbDecay = value;
            break;
    }
}

void Matrix::updateParameters()
{
    dist.setGain(distGain);
    dist.setThreshold(1 - distThreshold);

    phaser.set_lfo_frequency(pLfoFreq);
    phaser.set_feedback_depth(pFeedback);

    delay.setDelayLength(dTime);
    delay.setFeedback(dFeedback);

    if (verbRoomSize != prevRoomSize) {
        reverb.setParameter(MVerb<float>::SIZE, verbRoomSize);
        prevRoomSize = verbRoomSize;
    }
    if (verbDecay != prevDecay) {
        reverb.setParameter(MVerb<float>::DECAY, verbDecay);
        prevDecay = verbDecay;
    }
}

void Matrix::process(float *input, float **outputs, int frames)
{
    returns[0] = input;

    std::memset(sends[0], 0.0, sizeof(float)*frames);
    std::memset(sends[1], 0.0, sizeof(float)*frames);
    std::memset(sends[2], 0.0, sizeof(float)*frames);
    std::memset(sends[3], 0.0, sizeof(float)*frames);

    for (unsigned channel = 0; channel < MATRIX_SIZE; channel++) {

        if (channel < MATRIX_SIZE - 1) {
            envelopes[channel]->gate(range[channel]->withinRange(midiPitch));

            float env[1024];

            //XFADE function ==================================================
            //
            for (unsigned s = 0; s < frames; s++) {

                sends[matrixSettings[channel]][s] += (returns[channel][s] * 0.99);
                env[s] = envelopes[routingOrder[channel]]->process();
                float inputSample = sends[channel][s];

                switch((ModeEnum)CVmodes[channel])
                {
                    case postFading:
                        wetBuffer[channel][s] = inputSample;
                        dryBuffer[channel][s] = inputSample;
                        break;
                    case preFading:
                        wetBuffer[channel][s] = inputSample * env[s];
                        dryBuffer[channel][s] = inputSample * ((env[s] * -1) + 1);
                        break;
                }
            }

            //=================================================================
            float distLevel = 0.4;
            float phaserLevel = 0.9;
            float delayLevel = 2.0;
            float reverbLevel = 1.7;

            switch((EffectEnum)routingOrder[channel])
            {
                case Distortion:
                    dist.process(wetBuffer[channel], returns[channel + 1], frames);
                    for (unsigned f = 0; f < frames; f++) {
                        returns[channel + 1][f] *= distLevel;
                    }
                    break;
                case Phaser:
                    phaser.process(wetBuffer[channel], returns[channel + 1], frames);
                    for (unsigned f = 0; f < frames; f++) {
                        returns[channel + 1][f] *= phaserLevel;
                    }
                    break;
                case Delay:
                    delay.process(wetBuffer[channel], returns[channel + 1], frames);
                    for (unsigned f = 0; f < frames; f++) {
                        returns[channel + 1][f] *= delayLevel;
                    }
                    break;
                case Reverb:
                    reverb.process(wetBuffer[channel], returns[channel + 1], static_cast<int>(frames));
                    for (unsigned f = 0; f < frames; f++) {
                        returns[channel + 1][f] *= reverbLevel;
                    }
                    break;
            }

            for (unsigned s = 0; s < frames; s++) {

                //xfadesum function ==============================================================================================

                switch((ModeEnum)CVmodes[channel])
                {
                    case postFading:
                        returns[channel + 1][s] = (dryBuffer[channel][s] * ((env[s] * -1) + 1)) + (returns[channel + 1][s] * env[s]);
                        break;
                    case preFading:
                        returns[channel + 1][s] = dryBuffer[channel][s] + returns[channel + 1][s];
                        break;
                }
            }

            //================================================================================================================

        } else {

            for (unsigned f = 0; f < frames; f++) {
                sends[4][f] += returns[matrixSettings[channel]][f];

            }
            std::memcpy(outputs[0], sends[4],  sizeof(float)*frames);
            std::memset(sends[4], 0.0, sizeof(float)*frames);
        }
    }
}
