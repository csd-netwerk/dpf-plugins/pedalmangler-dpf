#ifdef __cplusplus
#ifndef _H_MATRIX_
#define _H_MATRIX_

#include "rangeCheck.hpp"
#include "ASR.hpp"
#include "simpledelay.hpp"
#include "simpleDistortion.hpp"
#include "stonephaser.hpp"
#include "MVerb.h"

#define MATRIX_SIZE 5
#define NUM_PARAMETERS 2

typedef enum {
    Distortion = 0,
    Phaser,
    Delay,
    Reverb,
    Output
}EffectEnum;

typedef enum {
    postFading = 0,
    preFading
}ModeEnum;

typedef enum {
    distortionGain = 0,
    distortionThreshold,
    phaserLfoFreq,
    phaserFeedback,
    delayTime,
    delayFeedback,
    reverbRoomSize,
    reverbDecay
}EffectParameters;

class Matrix {
public:
    Matrix();
    Matrix(float sampleRate);
    ~Matrix();
    void setSampleRate(float sampleRate);
    void setASR(float attackRate, float hold, float releaseRate, float mix, int fxLoop);
    void setFadeMethod(int value, int index);
    void setRoutingOrder(int value, int index);
    void setRange(int minimum, int maximum, int index);
    void setMatrix(int value, int index);
    void setMidiInput(int midiPitch);
    void updateParameters();
    void process(float *inputs, float **outputs, int frames);
    void updateParameters(float** parameters);
    void setEffectParameters(int index, float value);
    void setMode(unsigned paramNumber, unsigned value);
    void setFaderValue(unsigned paramNumber, unsigned value);
private:
    unsigned mode;

    int   CVmodes[4];
    float faders[MATRIX_SIZE];
    float modes[MATRIX_SIZE];

    RangeChecker **range;
    ASR   **envelopes;
    float **sends;
    float **returns;
    int matrixSettings[5] = {0,1,2,3,4};

    float send1[1024];
    float send2[1024];
    float send3[1024];
    float send4[1024];
    float send5[1024];

    float return1[1024];
    float return2[1024];
    float return3[1024];
    float return4[1024];
    float return5[1024];

    //effect classes
    SimpleDelay delay;
    SimpleDistortion dist;

    StonePhaser phaser;
    int bIndex = 0;
    double pSignal = 0.0;

    MVerb<float> reverb;

    //effect parameters
    float sampleRate;
    float distGain;
    float distThreshold;
    float pLfoFreq;
    float pFeedback;
    float dTime;
    float dFeedback;
    float verbRoomSize;
    float verbDecay;
    float prevRoomSize;
    float prevDecay;

    int midiPitch = 0;

    float dryBuffer[4][1024];
    float wetBuffer[4][1024];
    int routingOrder[4];
};


#endif //MATRIX
#endif //cplusplus
