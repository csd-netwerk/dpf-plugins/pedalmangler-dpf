#include "simpledelay.hpp"
#include <iostream>

SimpleDelay::SimpleDelay() {
    delayBuffer = new float[96000]();
}
SimpleDelay::~SimpleDelay() {
    delete [] delayBuffer;
    delayBuffer = nullptr;
}

void SimpleDelay::process(const float* input, float* output, int frames) {
    for(int i = 0; i < frames; i++) {
        output[i] = 0.5 * input[i] + 0.5 * delayBuffer[readhead];
        delayBuffer[writehead] = output[i] * this->fdbck;
        tick();
    }
}

void SimpleDelay::setDelayLength(int delay_ms) {
    this->delay_ms = delay_ms;
}
void SimpleDelay::setFeedback(float fdbck) {
    this->fdbck = fdbck;
}

int SimpleDelay::getDelayLength() const{
    return this->delay_ms;
}
float SimpleDelay::getFeedback() const{
    return this->fdbck;
}

void SimpleDelay::tick() {
    writehead++;
    writehead = writehead % 96000; //TODO: make variable
    readhead = 96000 + writehead - int(delay_ms*48000*0.001);
    readhead = readhead % 96000;
}
