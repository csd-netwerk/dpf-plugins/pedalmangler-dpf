#ifndef _H_RANGE_CHECKER_
#define _H_RANGE_CHECKER_

class RangeChecker 
{
public:
    RangeChecker();
    ~RangeChecker();

    void setRange(float min, float max);
    bool withinRange(float input);
protected:
    bool state = false;
    int range_min;
    int range_max;
};

#endif //_H_RANGE_CHECKER_
